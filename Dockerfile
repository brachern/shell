FROM node

COPY package.json .
RUN npm install -g tsc
RUN yarn
COPY . .

CMD PORT=$PORT node server.js
