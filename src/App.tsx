import * as React from "react";
import AppList from "./components/AppList/AppList";
import Features from "./components/Features/Features";
import Header from "./components/Header/Header";

class App extends React.Component {
  public render() {
    return (
      <div>
        <Header />
        <AppList />
        <Features />
      </div>
    );
  }
}

export default App;
