import * as React from "react";

const appList = {
  backgroundColor: "#1d1e22",
  display: "flex",
  float: "left" as "left",
  fontSize: "20px",
  height: "100vh",
  justifyContent: "center",
  textAlign: "center" as "center",
  width: "20%"
};

class AppList extends React.Component {
  public render() {
    return (
      <div style={appList}>
        <ul style={{ listStyle: "none", margin: "0", padding: "0" }}>
          <li style={{ padding: "10px" }}>
            <a href="#">Ecomm</a>
          </li>
          <li>
            <a href="#">SEM</a>
          </li>
        </ul>
      </div>
    );
  }
}

export default AppList;
