import * as React from "react";

const feature = {
  alignItems: "center",
  backgroundColor: "#9c398d",
  border: "1px solid white",
  color: "black",
  display: "flex",
  flexGrow: 1,
  fontSize: "1em",
  height: "50px",
  justifyContent: "center",
  padding: "10px",
  textAlign: "center" as "center",
  textDecoration: "none"
};

const features = { display: "flex", justifyContent: "space-evenly" };

class Features extends React.Component {
  public render() {
    return (
      <div style={features}>
        <a href="#" style={feature}>
          Findability
        </a>
        <a href="#" style={feature}>
          Keyword manager
        </a>
        <a href="#" style={feature}>
          Scorecard
        </a>
      </div>
    );
  }
}

export default Features;
