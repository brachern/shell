import * as React from "react";

const header = {
  backgroundColor: "black",
  color: "white",
  padding: "20px"
};

class Header extends React.Component {
  public render() {
    return (
      <div>
        <div style={header}>Kantar Media Rearchitecture</div>
      </div>
    );
  }
}

export default Header;
